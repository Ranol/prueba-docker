# Usamos ngnix alpine como imagen base
FROM nginx:1.21.4-alpine

# Copiamos la carpeta de nuestro host dentro de el contenedor Docker para utilizar el index.html
COPY ./html/index.html /usr/share/nginx/html/

# Damos permisos de lectura a index.html para evitar problemas
RUN chmod +r /usr/share/nginx/html/index.html

# Exponemos el puerto 80
EXPOSE 80

# Indicamos comando para inciar nginx con directrices globales y demonios desactivados
CMD ["nginx","-g","daemon off;"]
