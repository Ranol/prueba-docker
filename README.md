- PREGUNTA 1


Después de iniciar el Fork a partir del proyecto público del laboratorio, se solicita crear una receta Dockerfile que permita desplegar una página de inicio index.html con el contenido "Hola Zippy!". Esto lo realizamos de la siguiente manera:
1.- Añadimos un archivo index.html genérico con un párrafo "Hola Zippy!" dentro de la carpeta de nuestro proyecto
2.- Creamos un archivo Dockerfile. En este caso lo realizamos mediante el comando "vim Dockerfile", asegurándonos de antes estar situados dentro del directorio en el cual desarrollaremos el laboratorio (en este caso prueba-docker/).
3.- Añadimos el siguiente código a nuestro Dockerfile con cualquier editor de texto:


Usamos ngnix alpine como imagen base. Utilizamos la versión "1.21.4-alpine" de Dockerhub en vez de "latest" para evitar conflicto en caso de actualizaciones, además de ser una versión más liviana y limpia que la normal.
FROM nginx:1.21.4-alpine

Copiamos la carpeta de nuestro host dentro de el contenedor Docker para utilizar nuestro index.html personalizado, en vez del que nos ofrece nginx por defecto.
COPY ./html/index.html /usr/share/nginx/html/

Damos permisos de lectura a index.html para evitar cualquier problema problemas
RUN chmod +r /usr/share/nginx/html/index.html

Exponemos el puerto 80
EXPOSE 80

Indicamos comando para inciar nginx con directrices globales y demonios desactivados
CMD ["nginx","-g","daemon off;"]

4.- Confirmamos que estemos situados dentro de la carpeta "prueba-docker" en nuestra consola de comandos, y ejecutamos el comando "docker build -t nginx-hellozippy:1.0 . " para construir una imagen a partir de la receta Dockerfile que acabamos de crear según lo requerido.
-"nginx-hellozippy:1.0" es un nombre que podemos elegir según nuestra preferencia, pero se recomienda utilizar el mismo nombre para seguir estas instrucciones sin problemas, siendo "1.0" la versión de la imagen que estaremos creando.

- El punto final es sumamente importante, pues le estaremos indicando a la consola que el directorio donde se encuentra el Dockerfile desde el cual construir la imagen se encuentra situado en el mismo lugar que la consola de comandos.

- Después de finalizar el proceso, podemos revisar que la imagen que acabamos de crear esté correctamente instalada en Docker mediante el comando "docker image ls". Esto desplegará nuestra lista de imágenes, en la cual "nginx-hellozippy" debería aparecer en la parte superior.
5.- Iniciamos un contenedor basado en la imagen que acabamos de crear mediante "docker run -p 8080:80 nginx-hellozippy:1.0".
- Opcionalmente, podemos añadir además el argumento -d para correr nuestro contenedor en background.

- Podemos revisar el contenedor recientemente creado en la app de Docker, o bien, mediante el comando "docker ps". Con el ID que aparezca en esta lista, también podemos parar este contenedor ingresando "docker stop [id]" en el terminal.

- De nuevo, nginx-hellozippy:1.0 podría variar dependiendo del nombre que hayamos utilizado al crear nuestra imagen.
6.- Finalmente debemos ingresar a localhost:8080 en cualquier navegador, y visualizaremos una página genérica con el texto "Hello Zippy!".
______________________________________________________________________________

- PREGUNTA 2


En este ejercicio haremos uso de los volúmenes de Docker para replicar una carpeta de nuestro host dentro del contenedor. Debemos crear un contenedor nuevo con la imagen creada a partir del Dockerfile.

1.- Usaremos el comando "run -d -p 8080:80 -v ./html/:/usr/share/nginx/html nginx-hellozippy:1.0" asegurándonos de que el nombre de nuestra imagen creada en el paso anterior coincida con la que estamos indicando en el comando.

    - Nuevamente, deberemos fijarnos que estamos situados en la terminal dentro de la carpeta donde tenemos el proyecto.

    - Utilizaremos "-v" para indicar que estaremos haciendo uso de un volumen. Acto seguido, anotamos la carpeta de nuestro host que queremos que sea copiada dentro del contenedor nginx, en este caso, la carpeta que contiene nuestro index.html. Después de ":", indicamos la ruta donde se situan los archivos html en nuestro contenedor Docker de nginx.

Hecho esto, cualquier cambio que realicemos dentro de la carpeta html se verá reflejado en el contenedor mientras éste se encuentre inicializado. Para testearlo, podemos agregar un "!" adicional en el texto Hello Zippy, y refrescamos la página localhost:8080 para ver que se ha añadido el nuevo cambio.

